#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup

setup(
    name='xivo-call-logs',
    version='1.2',
    description='XiVO Call Logs Generation',
    author='Avencall',
    author_email='dev@avencall.com',
    url='https://github.com/xivo-pbx/xivo-call-logs',
    license='GPLv3',
    packages=find_packages(),
    scripts=['bin/xivo-call-logs', 'bin/xivo-call-logd'],
)
